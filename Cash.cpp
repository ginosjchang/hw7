#include "Cash.h"

Cash::Cash(int x){
    totalPrice = x;
    n[0] = x / 1000;
    x -= 1000 * n[0];
    n[1] = x / 500;
    x -= 500 * n[1];
    n[2] = x / 100;
    x -= 100 * n[2];
    n[3] = x / 50;
    x -= 50 * n[3];
    n[4] = x / 10;
    x -= 10 * n[4];
    n[5] = x / 5;
    x -= 5 * n[5];
    n[6] = x;
}

void Cash::show(){
    cout << " The total price is : " << totalPrice << endl;
}

void Cash::pay(int x){
    x -= totalPrice;
    for(int i = 0, N; i < 7; ++i)
    {
        switch(i)
        {
            case 0: N = 1000;break;
            case 1: N = 500;break;
            case 2: N = 100;break;
            case 3: N = 50;break;
            case 4: N = 10;break;
            case 5: N = 5;break;
            case 6: N = 1;break;
        }
        
        cout << N << " : " << x / N << endl;
        x -= N * (x / N);
    }
}
