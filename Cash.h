#ifndef CASH_H
#define CASH_H
#include <iostream>
using namespace std;
class Cash{
    public:
        Cash(int x);
        void pay(int x);
        void show();
    private:
        int n[7], totalPrice;

};

#endif
