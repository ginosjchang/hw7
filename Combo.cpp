#include "Combo.h"
#include "Cash.h"

Combo::Combo(){
    main = 1;
    combo = 'A';
    drink.setDrink(1);
    drink.setNoIce(true);
    custom_Count++;
}
Combo::Combo(int x, char y, int z){
    main = x;
    combo = y;
    drink.setDrink(z);
    drink.setNoIce(true);
    custom_Count++;
}
Combo::Combo(int x, char y, int z, bool w){
    main = x;
    combo = y;
    drink.setDrink(z);
    drink.setNoIce(w);
    custom_Count++;
}
void Combo::setMain(int x){
    main = x;
}
void Combo::setCombo(char y){
    combo = y;
}

void Combo::setDrink(int z, bool w){
    drink.setDrink(z);
    drink.setNoIce(w);
}

void Combo::setNoIce(bool w){
    drink.setNoIce(w);
}

Cash Combo::TotalPrice(){
    int x = 0;
    switch(main)
    {
        case 1: case 8: x += 72;break;
        case 2: x += 62;break;
        case 3: x += 99;break;
        case 4: x += 82;break;
        case 5: x += 44;break;
        case 6: x += 60;break;
        case 7: x += 100;break;
        case 9: x += 110;break;
        case 10: x += 90;break;
        case 11: x += 52;break;
        case 12: x += 44;break;
        case 13: x += 69;break;
        case 14: x += 109;break;
        case 15: x += 109;break;
        case 16: x += 109;break;
        case 17: x += 119;break;
    }
    switch(combo)
    {
        case 'A': case'a': x += 55;break;
        case 'B': case'b': x += 55;break;
        case 'C': case'c': x += 65;break;
        case 'D': case'd': x += 68;break;
        case 'E': case'r': x += 79;break;
    }

    Cash cash(x);

    return cash;

}

void Combo::show(){
    cout << "main : " << main << endl
        << " combo : " << combo << endl;
    drink.show();
}

int Combo::GetCustomCount(){return custom_Count;}

int Combo::custom_Count = 0;

