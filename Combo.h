#ifndef COMBO_H
#define COMBO_H
#include "Drink.h"
#include "Cash.h"
class Combo{
    public:
        Combo();
        Combo(int x, char y, int z);
        Combo(int x, char y, int z, bool w);

        //static int custom_Count;
        void setMain(int x);
        void setCombo(char y);
        void setDrink(int z, bool w);
        void setNoIce(bool w);
        Cash TotalPrice();
        void show();
        int GetCustomCount();
    private:
        static int custom_Count;
        int main;
        char combo;
        Drink drink;
};


#endif
