#include "Drink.h"

Drink::Drink(){
    drink = 1;
    noIce = true;
}

Drink::Drink(int x, bool y){
    drink = x;
    noIce = y;
}

void Drink::setDrink(int x){
    drink = x;
}

void Drink::setNoIce(bool y){
    noIce = y;
}

void Drink::show(){
    cout << "Drink :";
    switch(drink)
    {
        case 1: cout << " cola ";break;
        case 2: cout << " zero cola ";break;
        case 3: cout << " black tea ";break;
        case 4: cout << " green tea ";break;
        case 5: cout << " orange juice ";break;
    }
    cout << " ICE :" ;
    if(noIce == true) cout << "no\n";
    else cout << "yes\n";
}
