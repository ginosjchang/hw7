#ifndef DRINK_H
#define DRINK_H
#include <iostream>

using namespace std;

class Drink{
    public:

    Drink();
    Drink(int x, bool y);
    void setDrink(int x);
    void setNoIce(bool y);
    void show();

    private:

    int drink;
    bool noIce;


};
#endif
