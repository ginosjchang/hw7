hw7 : Combo.o  HW7.o Drink.o Cash.o
	g++ -o hw7 Combo.o HW7.o Drink.o Cash.o

hw7.o : HW7.cpp Combo.h
	g++ -c HW7.cpp

Combo.o :  Combo.cpp 
	g++ -c Combo.cpp 

Drink.o : Drink.cpp
	g++ -c Drink.cpp

Cash.o : Cash.cpp
	g++ -c Cash.cpp

clean :
	rm Cash.*o Drink.*o Combo.*o hw7.*o HW7.o
